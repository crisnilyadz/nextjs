// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default (req, res) => {
 

  const fs = require('fs');
  try{ 
  // json data
  var jsonData = '{"persons":[{"name":"John","city":"New York"},{"name":"Phil","city":"Ohio"}]}';
  
  // parse json
  var jsonObj = JSON.parse(jsonData);
  console.log(jsonObj);
  
  // stringify JSON Object
  var jsonContent = JSON.stringify(jsonObj);
  console.log(jsonContent);
  
  fs.writeFile("output.json", jsonContent, 'utf8', function (err) {
      if (err) {
          console.log("An error occured while writing JSON Object to File.");
          return console.log(err);
      }
  
      console.log("JSON file has been saved.");
  });

  res.statusCode = 200
  res.json({ name: 'JSON file has been saved' })
  }
  catch( e){
    res.statusCode = 500
    res.json({ name: 'JSON failed' })
  }
}
